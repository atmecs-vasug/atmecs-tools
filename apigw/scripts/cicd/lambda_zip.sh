#This is a script to prepare zip files for Lambada and Layer code
# input: $1 lambdas.in
#source $1
my_node_module=$2
my_code_root=$3
my_content_root=$4

echo "my_node_module="$my_node_module
echo "my_code_root="$my_code_root
echo "my_content_root="$my_content_root

temp=temp

#First prepare a temporary directory to create the zip files temporarily
tmp_dir=$temp
##for mk_dir in $lambda_names;
##do
##  mk_dir=`echo $mk_dir | sed 's/issuer_ticker_/issuer\/ticker\//g'`
##  mkdir -p $tmp_dir/$mk_dir
##done

# f3-f6 are NOT used in this script
while IFS=: read -r lambda_name lambda_path f3 f4 f5 f6;
do
  case $lambda_name in
    ''|\#*) continue ;;  # skip blank lines and lines starting with #
  esac
  echo "Lambda name:"$lambda_name" with path:"$lambda_path
  mkdir -p $tmp_dir/$lambda_path
done < "$1"
mkdir -p $tmp_dir/content_layer

#unzip the node_modules to temp directory
unzip $my_node_module -d $tmp_dir/content_layer/

#copy the content and qry_parser files into the content_layer directory
cp -r $my_content_root/*.js $tmp_dir/content_layer/

#now we have all the required files and node modules in content_layer and now mapping all the zip files to lambda functions
cd $tmp_dir; zip -r content_layer.zip content_layer/*; cd ..

#iterate through the necessary lambda directories and create a zip for each lambda files with the same function name
# f3-f6 are NOT used in this script
while IFS=: read -r lambda_name lambda_path f3 f4 f5 f6;
do
  case $lambda_name in
    ''|\#*) continue ;;  # skip blank lines and lines starting with #
  esac
  echo "Lambda name:"$lambda_name" with path:"$lambda_path
  zip -j ./$tmp_dir/$lambda_path/$lambda_name.zip $my_code_root/$lambda_path/*.js
done < "$1"

