#!/bin/bash
echo "this script is to delete the created lambda functions and api gateway along with usageplan"
export AWS_PROFILE=apigw_os
region=us-east-1

# $1 is config file config/env_onfly.cfg
source $1

#iterating the loop to get lambda names 
api_names_file=$scripts_root/cicd/lambdas.in
echo "<<API Names to process from file:"$api_names_file

#for lambda_del in $lambda_names;
while IFS=: read -r lambda_del f2 f3 f4 f5 f6
do
  case $lambda_del in
    ''|\#*) continue ;;  # skip blank lines and lines starting with #
  esac
  #appending the lambda prefix variable to lambda name
  fn_name=$fn_prefix$lambda_del

  #deleting the lambda functions
  echo aws lambda delete-function --region $region --function-name $fn_name
  aws lambda delete-function --region $region --function-name $fn_name

  echo "deleted lambda function" $fn_name
done < "$api_names_file"

#getting layer latest version which deployed on Onfly
layer_del=$(aws lambda list-layer-versions --region $region --layer-name $layer_name | grep $layer_name | sed -n '1p' | sed 's/.*"\(.*\)"[^"]*$/\1/' | sed 's/.*://')

#deleting deployed layer version from onfly
echo aws lambda delete-layer-version --region $region --layer-name $layer_name --version-number $layer_del
aws lambda delete-layer-version --region $region --layer-name $layer_name --version-number $layer_del
echo "deleted layer" $layer_name

#storing ApiId to a variable
apiid=`cat ./apiid.txt`

#storing usage_planid to a variable
usage_plan_id=`cat ./usageplan.txt`

#deleting api gateway stage
echo aws apigateway delete-stage --region $region --rest-api-id $apiid --stage-name $lambda_alias
aws apigateway delete-stage --region $region --rest-api-id $apiid --stage-name $lambda_alias
echo "deleted stage $lambda_alias associated with $api_name" 

#deleting rest api
echo aws apigateway delete-rest-api --region $region --rest-api-id $apiid 
aws apigateway delete-rest-api --region $region --rest-api-id $apiid 
echo "deleted ApiGateway" $api_name

#deleting usageplan
echo aws apigateway delete-usage-plan --region $region --usage-plan-id $usage_plan_id
aws apigateway delete-usage-plan --region $region --usage-plan-id $usage_plan_id
echo "deleted usage plan" $usage_plan_name

rm ./apiid.txt
rm ./usageplan.txt

