#!/bin/bash
echo "setup configuration based on the deployment mode:"$1
#calling the arguments: S1(env_onfly.cfg)
export AWS_PROFILE=apigw_os
echo "AWS Profile being used: "$AWS_PROFILE
source $1

echo "Code Root="$code_root
echo "Scripts Root="$scripts_root
echo "<<<In the Main onthefly script"
echo "onthefly="$on_the_fly
echo "region="$region
echo "layer_name="$layer_name
echo "fn_prefix="$fn_prefix
echo "iam_role="$iam_role

#define some common variables. These can be removed appropriatley via refactoring
#need to rename these temp vars to better names
temp=temp

#First prepare a temporary directory to create the zip files temporarily
source $scripts_root/cicd/lambda_zip.sh $scripts_root/cicd/lambdas.in ./aws/lambda/lib/node_modules.zip $code_root $content_root

#Now lets publish the lambda code/versions to AWS
source $scripts_root/cicd/lambda_publish.sh $scripts_root/cicd/lambdas.in 

echo "The below lines of script is to create the APIGateway"
source $scripts_root/cicd/api_onfly.sh


