#api_onfly.sh
echo ">>>Entered api_onfly.sh to handle API and Resource creation on the fly."

temp=temp
tmp_dir=$temp
if [ -d $tmp_dir ]; then
  echo "Temp directory already exists:"$tmp_dir
else
  mkdir $tmp_dir
fi

echo "OnTheFly="$on_the_fly
echo "region="$region
echo "fn_prefix="$fn_prefix
echo "api_name="$api_name

echo "The below lines of script is to create the APIGateway"
#API_Name=$api_name

#Getting existing rest api names
aws apigateway get-rest-apis --region $region | grep -i 'id\|name' > $tmp_dir/check.txt
APIName=`grep "$api_name" $tmp_dir/check.txt | grep name | sed 's/ //g' | sed 's/^[^:]*://g' | sed 's/ *$//' | sed 's/"//g' | sed 's/,//g'`

#validating
if [ "$api_name" == "$APIName" ]; then
  echo "API name already exist in API Gateway. Unable to Proceed for now."
  exit -1
fi

# We are ready to setup everything from scratch now
#creating API gateway
aws apigateway create-rest-api --region $region --name $api_name --description "$api_name" --binary-media-types "*/*" > $tmp_dir/test.txt
apiid=`grep 'id' $tmp_dir/test.txt | sed 's/ //g' | sed 's/^[^:]*://g' | sed 's/ *$//' | sed 's/"//g' | sed 's/,//g'`
echo "apiid="$apiid
 
#Getting root ParentId
ROOTPARENTID=`aws apigateway get-resources --region $region --rest-api-id $apiid | grep "\"id\":"| cut -d ":" -f2 | sed -e 's/^[[:space:]]*//g' | sed 's/"//g' | sed 's/,//g'`
echo "ROOTPARENTID="$ROOTPARENTID

#now, create /pathA and /pathA/{pathB} resources
#creating resources under root
aws apigateway create-resource --rest-api-id $apiid --region $region --parent-id $ROOTPARENTID --path-part "pathA" > $tmp_dir/api_pathA.txt
api_issuer_id=`grep 'id' $tmp_dir/api_pathA.txt | sed 's/ //g' | sed 's/^[^:]*://g' | sed 's/ *$//' | sed 's/"//g' | sed 's/,//g'`
aws apigateway create-resource --rest-api-id $apiid --region $region --parent-id $api_issuer_id --path-part "{pathB}" > $tmp_dir/api_pathA_pathB.txt
api_issuer_ticker_id=`grep 'id' $tmp_dir/api_pathA_pathB.txt | sed 's/ //g' | sed 's/^[^:]*://g' | sed 's/ *$//' | sed 's/"//g' | sed 's/,//g'`

# Read the list of api resource names and associate lamdas and create API GW setup
#api_names_file=$scripts_root/cicd/config/env_apis.cfg
api_names_file=$scripts_root/cicd/lambdas.in
echo "<<API Names to process from file:"$api_names_file

while IFS=: read -r f1 f2 f3 f4 f5 f6
do
  case $f1 in
    ''|\#*) continue ;;  # skip blank lines and lines starting with #
  esac
  #set parent id based on $1
  echo "Create API under Root:"$f4" Resource:"$f5
  if [ $f4 == '/' ]; then
    parent_id=$ROOTPARENTID
  elif [ $f4 == '/pathA' ]; then
    parent_id=$api_issuer_id
  elif [ $f4 == '/pathA/{pathB}' ]; then
    parent_id=$api_issuer_ticker_id
  fi
  #creating resources under root
  #echo aws apigateway create-resource --rest-api-id $apiid --region $region --parent-id $parent_id --path-part $f5 > $tmp_dir/api_resource.txt
  aws apigateway create-resource --rest-api-id $apiid --region $region --parent-id $parent_id --path-part $f5 > $tmp_dir/api_resource.txt

  #getting created resource id
  api_resource_id=`grep 'id' $tmp_dir/api_resource.txt | sed 's/ //g' | sed 's/^[^:]*://g' | sed 's/ *$//' | sed 's/"//g' | sed 's/,//g'`
  echo "api_resource_id="$api_resource_id

  if [ $f6 == 'GET' ]; then
    printf 'Root: %s, Resource: %s, Lambda: %s\n' "$f4" "$f5" "$fn_prefix$f1"
    #allocating method to the created resource
    aws apigateway put-method --rest-api-id $apiid --resource-id $api_resource_id --http-method GET --authorization-type "NONE" --api-key-required --region $region
    #getting lambda function arn
    #FunctionArn=`aws lambda list-functions --region $region | grep -i 'FunctionArn' | sed 's/"//g' | sed 's/^[^:]*://g' | sed 's/FunctionArn//g'| sed 's/,//g' | sed 's/ //' | sed 's/ *$//' | grep "$fn_prefix$f1" | awk -F":$fn_prefix*" '{print $1}'`
    FunctionArn=`aws lambda list-functions --region $region | grep -i 'FunctionArn' | sed 's/"//g' | sed 's/^[^:]*://g' | sed 's/FunctionArn//g'| sed 's/,//g' | sed 's/ //' | sed 's/ *$//' | grep "$fn_prefix$f1"`
    echo "FunctionArn="$FunctionArn

    #put integration to add the lambda function to get method
    echo aws apigateway put-integration --region $region --rest-api-id $apiid --resource-id $api_resource_id --http-method GET --type AWS_PROXY --integration-http-method POST --uri "arn:aws:apigateway:"$region":lambda:path/2015-03-31/functions/"$FunctionArn":\${stageVariables.lambda_alias}/invocations"
    aws apigateway put-integration --region $region --rest-api-id $apiid --resource-id $api_resource_id --http-method GET --type AWS_PROXY --integration-http-method POST --uri "arn:aws:apigateway:"$region":lambda:path/2015-03-31/functions/"$FunctionArn":\${stageVariables.lambda_alias}/invocations"

    echo $FunctionArn > $tmp_dir/arn.txt

    #getting aource arn to add permission
    SourceArn=`cat $tmp_dir/arn.txt | grep -i 'arn' | sed 's/"//g' | sed 's/^[^:]*://g' | sed 's/arn//g'| sed 's/,//g'| sed 's/ //' | sed 's/ *$//' | sed 's/lambda/execute-api/g' | awk -F':function' '{print $1}'`
    STATEMENTID=`date +%s`;

    #adding permissions to the created resource to access lambda function
    if [ $f4 == '/' ]; then
      echo aws lambda add-permission --function-name "$FunctionArn:$lambda_alias" --statement-id $STATEMENTID --action lambda:InvokeFunction --principal apigateway.amazonaws.com --source-arn "arn:$SourceArn:$apiid/*/GET/"$f5"" --region "$region"
      aws lambda add-permission --function-name "$FunctionArn:$lambda_alias" --statement-id $STATEMENTID --action lambda:InvokeFunction --principal apigateway.amazonaws.com --source-arn "arn:$SourceArn:$apiid/*/GET/"$f5"" --region "$region"
    else
      echo aws lambda add-permission --function-name "$FunctionArn:$lambda_alias" --statement-id $STATEMENTID --action lambda:InvokeFunction --principal apigateway.amazonaws.com --source-arn "arn:$SourceArn:$apiid/*/GET/*/*/"$f5"" --region "$region"
      aws lambda add-permission --function-name "$FunctionArn:$lambda_alias" --statement-id $STATEMENTID --action lambda:InvokeFunction --principal apigateway.amazonaws.com --source-arn "arn:$SourceArn:$apiid/*/GET/*/*/"$f5"" --region "$region"
    fi
  else
    printf 'Only Resource: %s\n' "$fn_prefix$f1"
  fi
done < "$api_names_file"

#deploying the stage
echo aws apigateway create-deployment --region $region --variables "$lb_alias" --rest-api-id $apiid --stage-name $lambda_alias
aws apigateway create-deployment --region $region --variables "$lb_alias" --rest-api-id $apiid --stage-name $lambda_alias

#creating usage plan
aws apigateway create-usage-plan --region $region --name $usage_plan_name --description "OnFly UsagePlan" --api-stages apiId=$apiid,stage=$lambda_alias --throttle burstLimit=100,rateLimit=100 --quota limit=500,offset=0,period=MONTH > $tmp_dir/usageplan.txt
usageplanid=`grep 'id' $tmp_dir/usageplan.txt | sed -n '1p' | sed 's/.*"\(.*\)"[^"]*$/\1/' | sed 's/.*://'`

#getting apikeyid
#api_key_id=`aws apigateway get-api-keys --region $region | awk 'BEGIN{RS="        {"} /'\"$api_key_name\"$'/{ print ",} "$0} ' | grep id | cut -d ":" -f2 | sed -e 's/^[[:space:]]*//g' | sed 's/"//g' | sed 's/,//g'`
api_key_id=`aws apigateway get-api-keys --region $region | grep -B 1 "\"$api_key_name\"" | grep id | cut -d ":" -f2 | sed -e 's/^[[:space:]]*//g' | sed 's/"//g' | sed 's/,//g'`
echo "api_key_id="$api_key_id

#assigning apikey to the created usageplan
echo aws apigateway create-usage-plan-key --region $region --usage-plan-id $usageplanid --key-type "API_KEY" --key-id $api_key_id
aws apigateway create-usage-plan-key --region $region --usage-plan-id $usageplanid --key-type "API_KEY" --key-id $api_key_id

#removing the created temp dir
#rm -rf $tmp_dir

#pushing apiid to a file
echo $apiid > ./apiid.txt

#pushing usageplanid to a file
echo $usageplanid > ./usageplan.txt

temp_api_url="https://$apiid.execute-api.$region.amazonaws.com/$lambda_alias"
echo "Execute APIs end-point="$temp_api_url
export api_url="$temp_api_url"
echo "##vso[task.setvariable variable=apiUrl]$api_url"
`sleep 30`

