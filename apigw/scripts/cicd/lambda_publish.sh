#This is a script to publish lambda and layer code to AWS
# Input: $1 lambdas.in file

temp=temp
tmp_dir=$temp

echo "OnTheFly="$on_the_fly
echo "region="$region
echo "layer_name="$layer_name
echo "fn_prefix="$fn_prefix
echo "iam_role="$iam_role

#Publishing the Content layer and uploading the Zip file into the Content layer
aws lambda publish-layer-version --region $region --layer-name $layer_name --zip-file fileb://$tmp_dir/content_layer.zip > $tmp_dir/content_arn.txt
#rm -rf content_layer.zip

#getting the LayerFuntionArn and assigning it to a variable
content_layer_arn=$(cat $tmp_dir/content_arn.txt | grep -i 'LayerVersionArn' | sed 's/"//g' | sed 's/^[^:]*://g' | sed 's/LayerVersionArn//g'| sed 's/,//g' | sed 's/ //' | sed 's/ *$//')

#iterate through the temporary directories and do upload, publish and update environment of the each lambda code.
# f3-f6 are NOT used in this script
while IFS=: read -r lambda_name lambda_path f3 f4 f5 f6;
do
  case $lambda_name in
    ''|\#*) continue ;;  # skip blank lines and lines starting with #
  esac
  echo "Lambda name:"$lambda_name" with path:"$lambda_path

  #declaring function name to a variable
  fn_name=$fn_prefix$lambda_name
  echo "Working on Lambda Func:"$fn_name
 
  #Now we have a function name and zip code location. prepare a aws cli command to create and upload lambda code
  if [ $on_the_fly == "true" ]; then
    echo aws lambda create-function --region $region --function-name $fn_name --zip-file fileb://$tmp_dir/$lambda_path/$lambda_name.zip --runtime nodejs8.10 --handler index.handler --role $iam_role
    aws lambda create-function --region $region --function-name $fn_name --zip-file fileb://$tmp_dir/$lambda_path/$lambda_name.zip --runtime nodejs8.10 --handler index.handler --role $iam_role
  fi 

  echo aws lambda update-function-code --region $region --function-name $fn_name --zip-file fileb://$tmp_dir/$lambda_path/$lambda_name.zip
  aws lambda update-function-code --region $region --function-name $fn_name --zip-file fileb://$tmp_dir/$lambda_path/$lambda_name.zip

  #attaching the Layers to the Lambda Functions
  echo aws lambda update-function-configuration --region $region --function-name $fn_name --layers $content_layer_arn
  aws lambda update-function-configuration --region $region --function-name $fn_name --layers $content_layer_arn

  #attaching the VPC to lambda funtions
  if [ $on_the_fly == "true" ]; then
      echo "Lambda UpdateFuncConfig to attach VPC is commented out"
##    aws lambda update-function-configuration --region $region --function-name $fn_name --vpc-config SubnetIds=["$subnetId"],SecurityGroupIds=["$security_groupId"] --timeout 59
  fi

  #Publishing the version to each function if any code change
  echo aws lambda publish-version --region $region --function-name $fn_name
  aws lambda publish-version --region $region --function-name $fn_name

  #Listing out the versions numbers of the fucntion and storing in a remporary file
  echo aws lambda list-versions-by-function --region $region --function-name $fn_name | grep Version > $tmp_dir/tmp_ver.txt
  aws lambda list-versions-by-function --region $region --function-name $fn_name | grep Version > $tmp_dir/tmp_ver.txt

  #Using temporary file to get the latest version number and assiging it to a variable
  VERSION=$(tac $tmp_dir/tmp_ver.txt | sed -n '1p' | sed 's/"//g' | sed 's/://g' | sed 's/Version //g'| sed 's/,//g' | sed 's/ *//' | sed 's/ *$//')

  #creating lambda alias
  if [ $on_the_fly == "true" ]; then
    aws lambda create-alias --region $region --function-name $fn_name --name $lambda_alias --function-version $VERSION
  fi

  #updating the environment to the latest verison number which we got in the above VERSION variable
  echo aws lambda update-alias --region $region --function-name $fn_name --function-version $VERSION --name "$lambda_alias" --region "$region"
  aws lambda update-alias --region $region --function-name $fn_name --function-version $VERSION --name "$lambda_alias" --region "$region"
done < "$1"

