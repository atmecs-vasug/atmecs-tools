const cs_content = require("/opt/content_layer/content.js");
const qry_parser = require("/opt/content_layer/qry_parser.js")
const transform = require("/opt/content_layer/transform.js");

exports.handler = (event, context, callback) => {
    console.log("the value of event is " + JSON.stringify(event));

    var params = qry_parser.getParams(event,callback);
    var query = prepareQuery(params);
    console.log("Query <" + query + ">");
    cs_content.getContent(query, transformResponse);

    //transform the response payload
    function transformResponse(error,data){
        if(error) callback(data);
        //send response in requested output format - JSON/XML
        transform.convertResponseFormat(data,event,callback);
    }
};

//prepare the GL query
function prepareQuery(params) {
    var query = '{ dummyQuery () }';
    return query;
}
