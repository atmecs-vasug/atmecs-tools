"use strict";

//THis is needed when the user implements backend server calls
//const request = require("request");
const moment = require("moment");

const statusCodes = {
    "Success": 200,
    "Unauthorized": 401,
    "Error": 500
}

exports.getContent = (query, callback) => {
    callContentApi(query, callback);
} //end getContent

function callContentApi(query, callback) {
   return callback(null, getResponse(statusCodes.Success));
} //end callContentApi

/* Generates response for pdf,json data formats and error messages */
function getResponse(statuscode, responseHeader = { 'Content-type': 'application/json' }) {
    var dummyValue = { method: "Lambda", call_time: moment().format()};
    return {
        statusCode: statuscode,
        headers: responseHeader,
        isBase64Encoded: false,
        body: JSON.stringify(dummyValue)
    };
}

