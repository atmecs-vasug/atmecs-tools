//main function called by Lambdas
function getParams (event,callback) {
    var api_key = getApiKey(event.headers);
    var num_rows = getRows(event["queryStringParameters"]);
    var skip = getSkip(event["queryStringParameters"]);
    return {
        api_key: api_key,
        num_rows: num_rows,
        skip: skip
    };
}

//the header params seems changing whether the call is from gateway or cURL
function getApiKey (headers) {
    if (headers == null) return "";

    if (headers["x-api-key"] != undefined) {
        return headers["x-api-key"];
    } else if (headers["X-api-key"] != undefined) {
        return headers["X-api-key"];
    } if (headers["X-Api-Key"] != undefined) {
        return headers["X-Api-Key"];
    } else {
        return "";
    }
}

//get the 'num_rows' value from query params
// default to 10000, when no 'num_rows' is specified
function getRows (queryParams) {
    const ROWS_DEFAULT = 10000;

    var num_rows = ROWS_DEFAULT;
    if (queryParams == null) return num_rows;

    console.log ("Event param num_rows=" + queryParams['num_rows']);    
    if (queryParams['num_rows'] != undefined) {
        num_rows =  queryParams['num_rows'];
    }
    //possible, users might have passed in 0 or -ve number, so set to default
    if (num_rows <= 0) {
        num_rows = ROWS_DEFAULT;
    }
    return num_rows; 
}

//get the 'skip' value from query params
function getSkip (queryParams) {
    const SKIP_DEFAULT = 0;

    var skip = SKIP_DEFAULT;
    if (queryParams == null) return skip;
 
    console.log ("Event param skip=" + queryParams['skip']);   
    if (queryParams['skip'] != undefined) {
        skip = queryParams['skip'];
    } 
    //possible, users might have passed in 0 or -ve number, so set to default
    if (skip <= 0) {
        skip = SKIP_DEFAULT;
    }

    return skip;
}

module.exports.getParams = getParams;
