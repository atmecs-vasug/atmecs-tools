// This is a utility library used by different lamdbas for transforming the graphql response JSON payload

var jsxml = require("js2xmlparser");


//convert the response to either JSON or XML format depending on Accept
function convertResponseFormat(data, event, callback) {
    if (event.headers.Accept == "application/xml") {
        var xmlResponse = jsxml.parse("Response", JSON.parse(data.body));
        data.body = xmlResponse;
        data.headers = { 'Content-type': 'application/xml' };
        callback(null, data);
    }
    else {
        callback(null, data);
    }
}

module.exports.convertResponseFormat = convertResponseFormat;