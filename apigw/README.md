# apigw - available on GitLab 
# Developed and Open-Sourced by: ATMECS, Inc (https://www.atmecs.com)

A tool to let users Expose the backend APIs via API Gateway. This tool is used to setup API Gateway on the fly.  This includes setting up the API Gateway, resources and hooking up the resource methods to the backend Lambda code. 

Useful for:
   - This tool might be useful if you'd like to quickly setup AWS API Gateway from scrtach, Run your test cases to make sure all the backend exposed APIs are running OK or not.

What is this?
   This project is a testing tool to:
        
    *   Create List of Lambdas and a simple sample code for each Lambda
            *  Utilizes a Lambda Layer code for common functionality
    *  Create API Gateway
        *   Create API
        *   Create Resources
        *   Creates GET methods for each resource
        *   Hooks up API Integration between APIGW Get method and the Lambda code
        *   Utilizes Stage variables for Demonstration Purpose


How to Use it?
    *   Read "HowTo.txt" file

Run:
    - ./scripts/cicd/onthefly.sh ./scripts/cicd/config/env_onfly.cfg

Cleanup:
    - ./scripts/cicd/onthefly_delete.sh ./scripts/cicd/config/env_onfly.cfg

To-Do:
   1) Look at the Issues section

